module.exports = {
  // printWidth: 120, // forces one-liners for everything less than x chars long
  tabWidth: 2,
  useTabs: false,
  semi: false,
  singleQuote: true,
  trailingComma: 'none',
  bracketSpacing: true,
  jsxBracketSameLine: false,
  arrowParens: 'avoid',
  proseWrap: 'never',
  'vue/component-name-in-template-casing': 'off',
  endOfLine: 'lf'
}
