module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/standard',
    'prettier',
    'prettier/standard',
    'prettier/vue'
  ],
  plugins: ['prettier'],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    'space-before-function-paren': ['warn', 'never'],
    'no-unused-vars': 'warn',
    'vue/name-property-casing': ['warn', 'PascalCase|kebab-case'],
    'vue/component-name-in-template-casing': 'off',
    'vue/no-unused-components': 'warn',

    'array-bracket-newline': ['warn', 'consistent'],
    'array-element-newline': ['warn', 'consistent'],
    'object-curly-newline': ['warn', { consistent: true }],
    'function-paren-newline': ['warn', 'consistent']
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
