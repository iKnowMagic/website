import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import '@/config/fontawesome'
import Buefy from 'buefy'

import 'buefy/dist/buefy.css'
import 'devextreme/dist/css/dx.common.css'
import 'devextreme/dist/css/dx.light.compact.css'
import '@/scss/main.scss'

Vue.config.productionTip = false

Vue.use(Buefy)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
