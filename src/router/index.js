import Vue from 'vue'
// import NProgress from 'nprogress'
import Router from 'vue-router'

// https://github.com/declandewet/vue-meta
import VueMeta from 'vue-meta'

import routes from '@/router/routes'

Vue.use(Router)
Vue.use(VueMeta, {
  keyName: 'page'
})

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'active',
  routes: routes,

  // Simulate native-like scroll behavior when navigating to a new
  // route and using back/forward buttons.
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

// Before each route evaluates...
router.beforeEach(async(routeTo, routeFrom, next) => {
  next()
})

// When each route is finished evaluating...
router.afterEach((routeTo, routeFrom) => {})

export default router
