import { loadPage } from '@/router/helpers/loadPage'

export default [
  {
    path: '/',
    name: 'home',
    component: loadPage('Home')
  }
]
